---
title: git subcommand <commit>
verticalSeparator: ^\-\-\-\-$
---

# git subcommand `<commit>`

note:
    🍿🚿

---

## Introduction

----

```sh
git reset --help
```

```txt
git reset [--soft | --mixed [-N] | --hard | --merge | --keep
] [-q] [<commit>]
```

`<commit>`

note:
    `[]` = optionel
    `<blabla>` = variable

---

## Cibler un commit

----

Comment cibler précisement un `commit` ?

<div class="fragment">

Via son hash

Exemple `36112cd981035cbc3efd711aff02c67eacd9a389`

```sh
git show 36112cd981035cbc3efd711aff02c67eacd9a389
```

</div>

----

<div style="font-size: 2em;">

⌨️

😰

</div>

note:
    > Flemme
    > Long à taper

----

Comment cibler précisement un `commit` `*` ?

`*` _sans taper les 40 caractères du hash_

<div class="fragment">

Via **le début** de son hash

Exemple `3611`

```sh
git show 3611
```

4 caractères _minimum_

</div>

----

Limite : cas ambigues

```txt
erreur : l'id court d'objet 3131 est ambigu
astuce: Les candidats sont :
astuce:   arbre 3131d642
astuce:   blob 3131d998
fatal : argument '3131' ambigu : révision inconnue ou chemin inexistant.
```

Solutions : ajouter des caractères

Exemple `3131d9`

---

🤖

----

Comment identifier _facilement_ des commits ?

<div style="display: flex; flex-direction: row; align-items: center;">

![](images/commitish/log_graph.png)

<div class="fragment">

* `branch`
* `tag`
* `remote branch`
* `HEAD`

</div>

</div>

----

Qu'est-ce qu'une `branch` ?

<div class="fragment">

Un "pointeur **mobile**" vers un `commit`

</div>

----

Qu'est-ce qu'un `tag` ?

<div class="fragment">

Un "pointeur **fixe**" vers un `commit`

</div>

----

Qu'est-ce que c'est `HEAD` ?

<div class="fragment">

Un "pointeur **mobile**" vers _**le**_ `commit` qui représente l'état courant

</div>

----

`HEAD`

<div style="font-size: 2em;">

⌨️

🤔

</div>

<div class="fragment">

```txt
\@    @ 
 |\  /|\
/ \  / \
```

`HEAD` == [[@]]

</div>

note:
    majuscule

----

Comment rechercher un `commit` ?

<div class="fragment">

* `:/mot`
* `":/des mots recherchés"`

```sh
git show ":/des mots recherchés"
```

</div>

----

Comment cibler ce `commit` ?

![](images/commitish/head.png)

<div class="fragment">

* `@`
* `main`
* `:/gamble`
* rien _(dans la plupart des commandes)_

```sh
git show @
git switch main
git reset :/gamble
git diff
```

</div>

----

Résumé : cibler un `commit`

|                  |                                           |
| :--------------- | :---------------------------------------- |
| Hash             | `36112cd981035cb`...                      |
| Début du hash    | `3611`                                    |
| Branche          | `ma-branche`                              |
| Tag              | `mon-tag`                                 |
| Recherche        | `":/mots recherchés"`                     |
|                  | `HEAD`               ou               `@` |
| Branche distante | `origin/sa-branche`                       |

---

## Modificateurs

Cibler relativement un `<commit>` par rapport à un autre `<commit>`

----

Comment cibler ce `commit` ?

![](images/commitish/head_previous_3.png)

_**sans** passer par la recherche_

<div class="fragment">

* `@~3`
* `main~3`

```sh
git diff main~3
```

</div>

----

Cibler # `commit`s en arrière par rapport à un autre `<commit>`

| #^ième^ `commit`s en arrière | Nombre       | Répétition    |
| :--------------------------- | :----------- | :------------ |
| 0^ième^                      | `<commit>~0` | `<commit>`    |
| 1^ième^                      | `<commit>~1` | `<commit>~`   |
| 2^ième^                      | `<commit>~2` | `<commit>~~`  |
| 3^ième^                      | `<commit>~3` | `<commit>~~~` |

----

`@~5`

==

`HEAD~2~~~1` ≈≈ `(((HEAD~2)~)~)~1`

==

`HEAD~2~2~` ≈≈ `((HEAD~2)~2)~`

----

![](images/commitish/head_previous_1.png)

<div class="fragment">

* `@~`
* `main~`
* `@~1`
* `main~1`

```sh
git reset --hard @~
```

</div>

----

![](images/commitish/merge_base_vieille_branche_recente_chapeau_2.png)

<div class="fragment">

* `@^2`
* `main^2`

```sh
git switch --detach @^2
```

</div>

----

`<commit>~1`

==

`<commit>^1`

----

`<commit>~2`

⚠️ != ⚠️

`<commit>^2`

----

Cibler la #^ième^ `branch` par rapport à un `<commit>` de merge

| #^ième^ `branch` |              |             |
| :--------------- | :----------- | :---------- |
| 1                | `<commit>^1` | `<commit>^` |
| 2                | `<commit>^2` |             |

----

1 == 0 🤷

`<commit>^1`

==

`<commit>^0`

----

![](images/commitish/merge_base_vieille_branche_recente_chapeau_1.png)

<div class="fragment">

* `@^1`
* `main^1`
* `@~1`
* `main~1`

```sh
git reset --hard @~1
```

</div>

----

![](images/commitish/merge_base_recente_branche_vieille_chapeau_2.png)

<div class="fragment">

* `@^2`
* `main^2`

```sh
git log --oneline main^2
```

</div>

----

![](images/commitish/merge_base_recente_branche_vieille_chapeau_1.png)

<div class="fragment">

* `@^1`
* `main^1`
* `@~1`
* `main~1`

```sh
git restore --source @~1 .
```

</div>

----

![](images/commitish/merge_no_ff_chapeau_2.png)

<div class="fragment">

* `@^2`
* `main^2`

```sh
git branch pull-request main^2
```

</div>

----

![](images/commitish/merge_no_ff_chapeau_1.png)

<div class="fragment">

* `@^1`
* `main^1`
* `@~1`
* `main~1`

```sh
git switch --create avant-la-pull-request main^1
```

</div>

----

![](images/commitish/merge_previous_2.png)

<div class="fragment">

* `@^2~2`
* `main^2~2`

```sh
git diff main^2~2
```

</div>

----

Comment cibler le commit de `ma-branche` ?

![](images/commitish/ma-branche.png)

<div class="fragment">

* `ma-branche` 😜
* `@~3^2`
* `main~3^2`

```sh
git switch ma-branche
```

</div>

---

Clavier ⌨️

|                 |       [[~]]        |       [[@]]        |
| :-------------- | :----------------: | :----------------: |
| Linux / Windows | [[Alt Gr]] + [[2]] | [[Alt Gr]] + [[0]] |
| Linux           | [[Shift]] + [[²]]  |                    |
| Mac             | [[Option]] + [[N]] |       [[@]]        |

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
