---
title: Git Merge Rebase (basique)
verticalSeparator: ^\-\-\-\-$
---

# Git

Merge

Rebase (basique)

---

## Organisation

1. Tour de table
1. Théorie
1. Pause
1. Pratique
1. Tour de table

---

## Introduction

* Disclaimer
* Tour de table
  * votre niveau de connaissance
    * honnete
    * sans jugement
  * vos attentes

note:
    > je ne suis pas Charles Xavier / Professor X : je ne suis pas télépathe
    >
    > donc si vous ne dites pas que vous ne comprennez pas, je ne peux pas le savoir
    >
    > il n'y a aucune questions stupides
    >
    > si vous ne dites pas que vous ne savez pas, vous n'apprendrez pas
    >
    > n'hésitez pas à demander tant que vous n'avez pas compris

---

## Merge

----

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature"
    checkout main
    commit id:"colleagues' work"
```

```sh
git switch main
git merge feature
```

<div class="fragment">

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work"
    merge feature tag:"main"
```

</div>

note:
    divergent branches

----

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work" tag:"main"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
```

```sh
git switch main
git merge feature
```

<div class="fragment">

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    commit id:"refactor"
    commit id:"add feature" tag:"main feature"
    branch feature
```

</div>

note:
    fast forward by default

----

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work" tag:"main"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
```

```sh
git switch main
git merge --no-ff feature
```

<div class="fragment">

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    merge feature tag:"main"
```

</div>

note:
    no fast forward

---

## Rebase

----

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work" tag:"main"
```

```sh
git switch feature
git rebase main
```

<div class="fragment">

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work" tag:"main"
    branch rebasing/feature
    cherry-pick id:"refactor"
    cherry-pick id:"add feature"
```

</div>

----

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
    checkout main
    commit id:"colleagues' work" tag:"main"
```

```sh
git switch feature
git rebase main
```

```mermaid
%%{init: {'theme':'dark'}}%%
gitGraph
    commit id:"some"
    commit id:"shared"
    commit id:"work"
    commit id:"colleagues' work" tag:"main"
    branch feature
    checkout feature
    commit id:"refactor"
    commit id:"add feature" tag:"feature"
```

---

## Pause

5min

---

## Pratique

note:
    exercice graph 1 + graph 2 => deviner les commandes
    ;
    exercice graph 1 + commandes => dessiner le graph

---

## Cloture

* Questions ?
* Tour de table des attentes
* ROTI
* Feedbacks directs / MP plus tard
  * qu'avez-vous aimé ?
  * qu'avez-vous moins / pas aimé ?
  * comment améliorer la prochaine fois ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
