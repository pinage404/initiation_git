---
title: General
verticalSeparator: ^\-\-\-\-$
---

# General

---

- [Historique](#historique)
- [Quoi ?](#quoi-)

---

## Historique

---

### Qui ? / Quand ? / Pour quoi ?

- Créé par <span class="fragment">Linus Torvald</span> en <span class="fragment">2005</span>
- Maintenu par <span class="fragment">[Junio `gitster` Hamano](https://github.com/gitster)</span>
- Utilisé pour <span class="fragment">le développement du kernel Linux 🐧</span>

----

### Pourquoi ?

BitKeeper est devenu payant 💰

note:
    - l'outil utilisé (avant que git existe) pour Linux
    - était closed source (à l'époque)
    - quelqu'un a été accusé de l'avoir rétro-engineeré pour créer un clone
    - il est devenu payant

---

## Quoi ?

<span class="fragment">Système de gestion de versions</span>

note:
    est-ce que vous connaissez d'autres outils ?

----

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart TB
    subgraph Décentralisé
        direction TB

        BitKeeper --> Git
        BitKeeper --> Mercurial
    end

    subgraph ClientServer[Client Serveur]
        direction TB

        CVS[Concurrent Versions System\nCVS] --> SVN[Apache Subversion\nSVN]
    end
```

----

### Différence protocol VS service

<div class="fragment">

- <span class="fragment">Git != GitHub & GitLab & BitBucket & Serveur Gitea autohébergé …</span>
- <span class="fragment">Email != GMail & Outlook & Serveur Roundcube autohébergé …</span>

</div>

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
