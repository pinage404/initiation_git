---
title: Remote
verticalSeparator: ^\-\-\-\-$
---

# Remote

---

> Seul, on va plus vite.
>
> Ensemble, on va plus loin.

---

- [Théorie](#théorie)
- [Astuces](#astuces)

---

## Théorie

---

### Quoi ?

----

_Une_ manière de **partager** des **modifications** dans un repo entre pair

note:
    [patch email](https://github.com/torvalds/linux/pull/17#issuecomment-5654674)

----

remote ~= alias + tracking

<div class="fragment">

- SSH
- HTTPS
- File
- […](https://git-scm.com/docs/git-fetch#_git_urls)

</div>

---

### `origin`

décentralisé

---

### PR / Pull Request

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart LR
    subgraph GitHub
        subgraph Vision["🤖 Vision"]
            VisionRemote[Vision/Repo]

            Upstream>Upstream]
        end

        subgraph Wanda["🦸‍♀️ Wanda"]
            WandaRemote[Wanda/Repo]

            Fork>Fork]
        end
    end

    subgraph MachineWanda["Machine 🦸‍♀️ Wanda"]
        WandaClone[Repo]

        Clone>Clone]
    end

    VisionRemote -->|"Pull / fork 🦸‍♀️ Wanda"| WandaRemote
    WandaClone -->|"push origin"| WandaRemote
    WandaRemote -.->|"Request / origin"| VisionRemote
```

---

### MR / Merge Request

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart LR
    subgraph GitLab
        subgraph Wanda["🦸‍♀️ Wanda"]
            WandaRemote[Wanda/Repo]
        end
    end

    subgraph MachineWanda["Machine 🦸‍♀️ Wanda"]
        WandaRepo[Repo]
    end

    WandaRemote -->|"Merge Request / branch"| WandaRemote
    WandaRepo -->|"push origin"| WandaRemote
```

----

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart TB
    subgraph GitLab
        direction TB

        GitLabRepo[Repo]
    end

    subgraph MachineVision["Machine 🤖 Vision"]
        direction TB

        VisionRepo[Repo]
    end

    subgraph MachineWanda["Machine 🦸‍♀️ Wanda"]
        direction TB

        WandaRepo[Repo]
        WandaRepoCopy[Repo Copy]
    end

    VisionRepo --->|"git@gilab.com:user/repo.git"| GitLabRepo
    WandaRepo --->|"httsp://gilab.com/user/repo.git"| GitLabRepo

    VisionRepo -->|"vision@wanda.net:repo_copy.git"| WandaRepoCopy

    WandaRepo -->|"wanda@vision.net:repo.git"| VisionRepo
    WandaRepo -->|"../repo_copy"| WandaRepoCopy
```

<div class="fragment">

```sh
git push --set-upstream origin new-feature
```

</div>

---

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        clone_K((K))
        ==> clone_C

        main[main]
        main --> clone_D

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_D

        feature[feature]
        feature --> clone_H

        remotes_origin_feature{{origin/feature}}
        remotes_origin_feature --> clone_H

        remotes_origin_fix{{origin/fix}}
        remotes_origin_fix --> clone_K
    end

    remotes_origin_main -.-> origin_main

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug,remotes_origin_feature,remotes_origin_fix remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<div class="fragment">

```sh
git fetch
```

</div>

----

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_F((F))
        ==> clone_E((E))
        ==> clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        clone_K((K))
        ==> clone_C

        main[main]
        main --> clone_D

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_E

        remotes_origin_bug{{origin/bug}}
        remotes_origin_bug --> clone_F

        feature[feature]
        feature --> clone_H

        remotes_origin_feature{{origin/feature}}
        remotes_origin_feature --> clone_H

        remotes_origin_fix{{origin/fix}}
        remotes_origin_fix --> clone_K
    end

    remotes_origin_main -.-> origin_main
    remotes_origin_bug -.-> origin_bug

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug,remotes_origin_feature,remotes_origin_fix remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<div class="fragment">

```sh
git fetch --prune
```

</div>

----

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_F((F))
        ==> clone_E((E))
        ==> clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        main[main]
        main --> clone_D

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_E

        remotes_origin_bug{{origin/bug}}
        remotes_origin_bug --> clone_F

        feature[feature]
        feature --> clone_H
    end

    remotes_origin_main -.-> origin_main
    remotes_origin_bug -.-> origin_bug

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

<div class="fragment">

```sh
git pull
```

</div>

----

```mermaid
%%{init: {'theme':'dark'}}%%
flowchart TB
    subgraph origin
        origin_F((F))
        ==> origin_E((E))
        ==> origin_D((D))
        ==> origin_C((C))
        ==> origin_B((B))
        ==> origin_A((A))

        origin_main[main]
        origin_main --> origin_E

        origin_bug[bug]
        origin_bug --> origin_F
    end

    subgraph Clone
        clone_F((F))
        ==> clone_E((E))
        ==> clone_D((D))
        ==> clone_C((C))
        ==> clone_B((B))
        ==> clone_A((A))

        clone_H((H))
        ==> clone_B

        main[main]
        main --> clone_E

        remotes_origin_main{{origin/main}}
        remotes_origin_main --> clone_E

        remotes_origin_bug{{origin/bug}}
        remotes_origin_bug --> clone_F

        feature[feature]
        feature --> clone_H
    end

    remotes_origin_main -.-> origin_main
    remotes_origin_bug -.-> origin_bug

    classDef remotes stroke:red,color:red;
    class remotes_origin_main,remotes_origin_bug remotes;

    classDef commit stroke:royalblue,color:royalblue;
    class origin_A,origin_B,origin_C,origin_D,origin_E,origin_F,clone_A,clone_B,clone_C,clone_D,clone_E,clone_F,clone_H,clone_K commit;
```

---

## Astuces

---

### `fetch` != `pull`

`fetch` + `merge` = `pull` <!-- .element: class="fragment" -->

`fetch` + `rebase` = `pull --rebase` <!-- .element: class="fragment" -->

----

[Config](https://gitlab.com/pinage404/dotfiles/-/blob/56c2faadeeb1e531229db77109ad82d84ecd714d/dotfiles/config/git/better_default_behavior.gitconfig#L8)

```sh
git config --global pull.rebase merges
```

---

<div class="fragment" data-fragment-index="1">

### `--set-upstream`

</div>

```sh
git push
```

<div class="fragment" data-fragment-index="1">

```txt
fatal : La branche courante new-feature n'a pas de branche amont.
Pour pousser la branche courante et définir la distante comme amont, utilisez

    git push --set-upstream origin new-feature

Pour que cela soit fait automatiquement pour les branches sans
suivi distant, voir "push.autoSetupRemote' dans 'git help config'.
```

</div>

----

[Config](https://gitlab.com/pinage404/dotfiles/-/blob/56c2faadeeb1e531229db77109ad82d84ecd714d/dotfiles/config/git/better_default_behavior.gitconfig#L10)

```sh
git config --global push.autoSetupRemote true
```

---

### `(fetch)` / `(push)`

```sh
git remote --verbose
```

<div class="fragment">

```txt
origin  git@gitlab.com:pinage404/initiation_git.git (fetch)
origin  git@gitlab.com:pinage404/initiation_git.git (push)
```

</div>

----

### Repo publique

<div class="fragment">

```sh
git remote set-url \
    origin https://gitlab.com/pinage404/initiation_git.git
git remote set-url --push \
    origin git@gitlab.com:pinage404/initiation_git.git
```

```sh
git remote --verbose
```

```txt
origin  https://gitlab.com/pinage404/initiation_git.git (fetch)
origin  git@gitlab.com:pinage404/initiation_git.git (push)
```

</div>

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
