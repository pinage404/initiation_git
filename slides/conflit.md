---
title: Conflit
verticalSeparator: ^\-\-\-\-$
---

<span style="font-size: 20vmax">

📖

</span>

----

```sh
git switch main
```

```python
def fizzBuzz(number: int) -> str:
    return f"{number}"
```

```sh
git add --all
git commit
```

----

<div style="display: flex;">

<div class="fragment">

🦸‍♀️ Wanda

```sh
git switch --create fizz
```

```python
def fizzBuzz(number: int) -> str:
    if number % 3 == 0:
        return "Fizz"
    return f"{number}"
```

```sh
git commit --all
```

</div>

<div class="fragment">

🤖 Vison

```sh
git switch --create buzz
```

```python
def fizzBuzz(number: int) -> str:
    if number % 5 == 0:
        return "Buzz"
    return f"{number}"
```

```sh
git commit --all
```

</div>

</div>

----

![](images/conflit/graph_before_merge.png)

----

```sh
git switch main
git merge fizz
```

![](images/conflit/graph_before_conflict.png)

----

```sh
git merge buzz
```

<div class="fragment">

```txt
Fusion automatique de fizzbuzz.py
CONFLIT (contenu) : Conflit de fusion dans fizzbuzz.py
Pré-image enregistrée pour 'fizzbuzz.py'
La fusion automatique a échoué ; réglez les conflits et validez le résultat.
```

(╯°□°)╯︵ ┻━┻ <!-- .element: class="fragment" -->

💥💻🔥 <!-- .element: class="fragment" -->

₍₍ ᕕ(´◕⌓◕)ᕗ⁾⁾ <!-- .element: class="fragment" -->

</div>

---

# Gestion des conflits

----

<div class="fragment strike" data-fragment-index="1">

😠🗯️🗱🗮😡

CNV

</div>

<div class="fragment" data-fragment-index="1">

**`git`**

<small>🤡</small>

</div>

---

## Qu'est-ce qu'un conflit ?

----

### Sémantique

<span class="fragment" data-fragment-index="1">😠🗯️🗱🗮😡</span>

<span class="fragment strike" data-fragment-index="1">Conflit</span>
<span class="fragment" data-fragment-index="1">😱💭</span>

<span class="fragment" data-fragment-index="1">**Divergence** 🙂</span>

----

### Divergence

* _empêchant_ la **fusion automatique** 🚫🤖
* _demandant_ une **validation humaine** 🤔

🚫🤖→🤔

---

## Quand est-ce qu'un conflit se produit ?

----

Quand on **ré-applique** un **changement de code** qui impacte une partie du code qui a **divergé** entre :

* la `HEAD` sur laquelle le changement était basé
* la `HEAD` actuelle

----

![](images/conflit/graph_before_conflict.png)

----

### Quelles commandes peuvent produire des conflits ?

<div class="fragment">

* `merge`
  * `pull`
* `cherry-pick`
  * `rebase`
* `revert`
* `stash apply`
  * `stash pop`
* ...

</div>

---

## À quoi ressemble un conflit ?

----

```python
def fizzBuzz(number: int) -> str:
<<<<<<< HEAD
    if number % 3 == 0:
        return "Fizz"
=======
    if number % 5 == 0:
        return "Buzz"
>>>>>>> buzz
    return f"{number}"
```

note:
    git est comme sur Linux : tout est fichier

----

### Marqueurs de conflit

```txt
<<<<<<<

=======

>>>>>>>
```

----

```txt
<<<<<<< HEAD

=======

>>>>>>> buzz
```

----

```txt
<<<<<<< HEAD (Modification actuelle)

======= (séparateur de la divergence)

>>>>>>> buzz (Modification entrante)
```

----

```txt
<<<<<<< HEAD (Modification actuelle)

code existant sur HEAD

======= (séparateur de la divergence)

code ré-appliqué

>>>>>>> buzz (Modification entrante)
```

----

```python
def fizzBuzz(number: int) -> str:
<<<<<<< HEAD (Modification actuelle)
    if number % 3 == 0:
        return "Fizz"
======= (séparateur de la divergence)
    if number % 5 == 0:
        return "Buzz"
>>>>>>> buzz (Modification entrante)
    return f"{number}"
```

---

## Comment résoudre un conflit ?

----

1. Faire un choix entre les 2 versions
1. Potentiellement, faire un mélange des deux
1. Supprimer les marqueurs de conflits
1. `git add <path>`
1. `git <subcommand> --continue`
   * ou `git commit`

----

```python
def fizzBuzz(number: int) -> str:
    if number % 3 == 0:
        return "Fizz"
    if number % 5 == 0:
        return "Buzz"
    return f"{number}"
```

```sh
git add fizzbuzz.py
```

----

```sh
git merge --continue
```

<div class="fragment">

![](images/conflit/graph_after_merge.png)

┬─┬ノ( º _ ºノ)

</div>

---

<div style="font-size: 2em;">

⌨️

😰

</div>

----

## Tips

----

### mergetool

[`~/.gitconfig`](https://gitlab.com/pinage404/dotfiles/-/blob/56c2faadeeb1e531229db77109ad82d84ecd714d/dotfiles/config/git/vscode.gitconfig#L10)

```toml
[merge]
    tool = vscode
    guitool = vscode
[mergetool "vscode"]
    cmd = code --wait --merge $REMOTE $LOCAL $BASE $MERGED
```

----

```sh
git mergetool
```

![](images/conflit/vscode_mergetool.png)

----

### rerere

----

**Re**_use_ **Re**_corded_ **Re**_solution_

Permet d'_en_**re**_gistrer_ une **ré**_solution_ de conflit une fois et de la **ré**-_utiliser_ pour les fois suivantes <!-- .element: class="fragment" -->

[delicious-insights.com/fr/articles/git-rerere/](https://delicious-insights.com/fr/articles/git-rerere/) <!-- .element: class="fragment" -->

---

Questions ?

<!-- markdownlint-disable -->
<p class="legal">
    <a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">
        <img alt="Licence Creative Commons BY-NC-SA" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" />
    </a>
    by
    <a rel="author external" href="https://wheretofind.me/@pinage404">
        <img alt="pinage404" src="https://s.gravatar.com/avatar/0a7d96df27d5d020cb0d03340e734180?s=40" />
    </a>
    on
    <a rel="alternate" href="https://gitlab.com/pinage404/initiation_git">
        <img alt="GitLab" src="https://img.shields.io/gitlab/stars/pinage404/initiation_git?style=social" />
    </a>
</p>
<style>
.legal img {
    margin: 0;
    vertical-align: middle;
}
</style>
